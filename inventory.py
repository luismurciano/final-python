#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

#TODO: RECODE ENTIRELY
from utility import StatsContainer, StatsDict
import logging
__SLOTS__ = ['head','hand1','hand2','chest','shoulders','legs','gloves','feet','neck','ring1','ring2']
class Inventory(StatsContainer):
    def __init__(self):
        super(Inventory, self).__init__(allows = __SLOTS__)
          

#Execute if this file is run as a script and not imported as a module
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)
    inv = Inventory()
    d3 = StatsDict(str = 2,mag = 6, hp = 100)
    d4 = StatsDict(str = 2,mag = -12, hp = 100, res = 1)
    inv['head'] = d3
    inv['hand1'] = d4
    del inv['hand1']
    inv2 = inv + inv
    logging.info(inv2)