import random

class Battle (dict):
    """Class defining to teams engaging """
    def __init__(self, battle, scenario = 'default'):
        self = dict.__init__()
        self['battle'] = battle
        self['teams'] = []
        self['scenario'] = scenario

    def info(self):
        """Prints battle information """
        print self['battle']

class Player(dict):
    """This class stores players information from db during the battle """

    def __init__(self, pinfo):
        self['base_attribs'] = pinfo['base_attribs']
        self['items'] = pinfo['items']
        self['commands'] = {}
        self['attribs'] = self.update_attribs()
        self['buffs'] = []
        self['debuffs'] = []
        
    def update_attribs(self):
        """
        TODO: Implementar la actualizacion de atributos mediante items
        """
        return self['base_attribs']
    
    def add_command(self,command):
        cmd = command(self)
        self['commands'][ cmd['name'] ] = cmd
        return True
        
    def use_command(self,command_name,targets):
        """
        Calls and command using an Array of Targets
        """
        effect = None
        try:
            command = self['commands'][command_name]
        except KeyError:
            print "The command %s doesn`t exist" % command_name
        else:   
            if command.can_use():
                print "Using %s:" % command_name
                effect = command.execute(targets)
            else:
                print "You can't use %s " % command_name
                
        return effect
    
    def armor_mitigation(self):
        """
        Returns the factor of damage dealt once armor applied
        @param armor: Amount of armor
        @type armor: int
        @return: C{float}"""
        armor = self['attribs']['armor']
        return (100.0 / (100.0 + armor))

    def calculate_physical_dmg(self, target):
        """
        Returns the amount of damage dealt by the player
        """
        damage = self['attribs']['attack'] * target.armor_mitigation()
        return damage
    
    def turn(self):
        """
        Secuencias de acciones en el turno
        TODO: Analizar correctamente las fases
        """
        self.do_init_phase()
        self.do_command_phase()
        self.do_finish_phase()
        
    def  do_init_phase(self):
        """
        TODO: Implementar funciones antes del turno
        """
        return True
    
    def  do_command_phase(self):
        """
        TODO: Implementar la realizacion de comandos
        """
        return True
    
    def  do_finish_phase(self):
        """
        TODO: Implementar funciones de final del turno
        """
        return True
    
class Command(dict):
    """The Command Abstract class"""

    def __init__(self):
            pass
            #Make changes

    def execute(self):
            #OVERRIDE
            pass

class ActionCommand(Command):
    """The command Interface for player actions """

    def __init__(self, source):
        self['name'] = 'default'
        self['source'] = source
        self['description'] = 'None'
        self['cost'] = None
        self['cooldown'] = 0
        self['last_used'] = None
        
    def can_use(self):
        """
        TODO: Implentent the cooldown and status/debuff test
        """
        return True
    
    def execute(self,targets):
        pass

class PhysicalAttack(ActionCommand):
    """Action command, is the father of all pythiscal damaging abilities """

    def __init__(self, source):
        ActionCommand.__init__(self, source)
        self['name'] = 'Attack'

    def hits(self):
        """returns True if hit and False if not hit """
        return self.source['hit'] < random.randint(0, 99)

    def dodges(self):
        return self.target['dodge'] < random.randint(0, 99)

    def calculatePhysicalDmg(self):
        mitigation = ( 100.0 / ( 100.0 + self.target['armor'] ) )
        dmg = self.source['attack'] * mitigation

        return dmg

    def execute(self,targets):
        
        self.source.OnAttack()
        self.target.OnBeingAttack()

        if self.hits() and self.source.OnHit() and self.target.OnBeingHit():

            if self.dodges() and\
             self.target.OnDodge() and \
             self.source.OnBeingDodge():

                dmg = self.calculatePhysicalDmg()
                if (dmg > 0) and \
                self.target.OnDamage(dmg) and \
                self.source.OnBeingDamage(dmg):

                    pass    
class ITrigger(object):
    def __init__(self):
        pass

    def __call__(self):
        print "hello from ITrigger"


class OnAttack(ITrigger):
    """
    La instancia de este objeto debe ser llamado cuando un jugador ataca
    """
    def __init__(self, player):
        pass


class Switch:
    """ The INVOKER class"""
    def __init__(self, flipUpCmd, flipDownCmd):
        self.__flipUpCommand = flipUpCmd
        self.__flipDownCommand = flipDownCmd

    def flipUp(self):
        self.__flipUpCommand.execute()

    def flipDown(self):
        self.__flipDownCommand.execute()


class Light:
    """The RECEIVER Class"""

    def turnOn(self):
            print "The light is on"

    def turnOff(self):
            print "The light is off"




class Buff(object):
    """Buff and Debuff Interface """
    def __init__(self):
        #TODO: Implementar buff y uno de ejemplo
        pass
    
class FlipUpCommand(Command):
    """The Command class for turning on the light"""

    def __init__(self, light):
        self.__light = light

    def execute(self):
        self.__light.turnOn()


class FlipDownCommand(Command):
    """The Command class for turning off the light"""

    def __init__(self, light):
            Command.__init__(self)
            self.__light = light

    def execute(self):
        self.__light.turnOff()


class LightSwitch:
    """ The Client Class"""

    def __init__(self):
            self.__lamp = Light()
            self.__switchUp = FlipUpCommand(self.__lamp)
            self.__switchDown = FlipDownCommand(self.__lamp)
            self.__switch = Switch(self.__switchUp, self.__switchDown)

    def switch(self, cmd):
            cmd = cmd.strip().upper()
            try:
                if cmd == "ON":
                    self.__switch.flipUp()
                elif cmd == "OFF":
                    self.__switch.flipDown()
                else:
                    print "Argument \"ON\" or \"OFF\" is required."
            except Exception, msg:
                print "Exception occured: %s" % msg

# Execute if this file is run as a script and not imported as a module

if __name__ == "__main__":
    def a():
        print 'a'
        return True

    def b():
        print 'b'
        return True

    if 1 and a() and b():
        print "done"
    lightSwitch = LightSwitch()
    print "Switch ON test."
    lightSwitch.switch("ON")
    print "Switch OFF test"
    lightSwitch.switch("OFF")
    print "Invalid Command test"
    lightSwitch.switch("****")
    
    pinfo = {}
    pinfo['base_attribs'] = {'strenth':70}
    pinfo['items'] = {'axe':{'damage':12}}
    pinfo['commands'] = {}
    
    player1 = Player(pinfo)
    player1.add_command(PhysicalAttack)
    
    player2 = Player(pinfo)
    player2.add_command(PhysicalAttack)
    print player1.use_command('Attack', [])

    
    t = ITrigger()
    t()
