import os
from utility import AdditiveDict
from utility import StatsDict

__sell_rate__ = .33     #rate at which items sell by

#basic items template
class Item(object):
    def __init__(self, name):
          
        self.name = name
        
        if name == "None" or "":
             return None
        
        self.buy_price = 0                                               #value at which the item is bought
        self.sell_price = self.buy_price*__sell_rate__                        #value at which it is sold
        self.description = ""
                                                                        #item description used in shops and menus
   
class Equipable(Item, StatsDict):
    def __init__(self, name,stats = {}):
        Item.__init__(self, name)
        StatsDict.__init__(self,stats)
        if self.name == "None" or "":
             return None
        self.type = None
        self.slot = None
        self.weigth = 0
    
    def __str__(self):
        return 'Item %s \nStats:\n %s' % (self.name, StatsDict.__str__(self))
     

#items usable only in battle
class Usable(Item):
    def __init__(self, name):
        super(Usable, self).__init__(name)
        
        if self.name == "None" or "":
             return None
        
        self.function = eval()
            
#foods can function like usable items but only from the inventory
class Food(Item):
    def __init__(self, name):
        super(Food, self).__init__(name)
        
        if self.name == "None" or "":
             return None
        #TODO:
        self.function = eval()
            
#loot are items found in dungeons or are drops from specific monsters
#loot selling price is the same as it's worth which is different from 
# generic items where the selling price is 1/3.
# loot can not be bought from stores.
class Loot(Item):
    def __init__(self, name):
        super(Loot, self).__init__(name)
        
        self.sell_price = self.buy_price
            
if __name__ == "__main__":
    d1 = Equipable('Sheild',{'str':1,'def':100})
    print d1