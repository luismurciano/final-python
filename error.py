#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
import logging
class ActionStoppedError(Exception):
    
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr(self.value)

        
class InvalidSlotError(Exception):
    
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr('The Key: %s isn\'t allowed by AdditiveDict' % self.value)

class SubtractionError(Exception):
    
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr('Value %s can\'t be used in a subtraction' % self.value)
                        
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)
    try:
        raise ActionStoppedError('Boom! HEADSHOT!!!!')
        print 'esto no debe salir'
    except ActionStoppedError:
        print 'pues vaya'
        