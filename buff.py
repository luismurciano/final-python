#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
from utility import StatsDict
from utility import StatsContainer
from utility import ALLOWED_STATS
#import command
import logging
ALLOWED_TRIGGERS = ['on_cast', 'on_focus',
                   'on_damage', 'on_damaged',
                   'on_heal', 'on_healed',
                   'on_dodge', 'on_dodged',
                   'on_miss', 'on_missed',
                   'on_hit', 'on_hitted']

class BuffDict(StatsContainer):
    def __init__(self):
        super(BuffDict, self).__init__(allows = None)
          

class Buff(StatsDict):
    def __init__(self,stats = {}, parent = None, target = None):
        self.allows = ALLOWED_STATS + ALLOWED_TRIGGERS
        super(Buff, self).__init__( Dict = stats)
        #TODO: Implementar Buffs y uno de ejemplo
        self.name = None
        self.type = 'Buff'
        self.description = None
        self.parent = parent
        self.target = target
        self.done = False
        self.duration = None
    
    def update_buff(self):
        """ Buffs is updated at the end of the targets turn
            DeBuff idont know
            TODO: decide when to update
        """
        if self.duration != None:
            if self.duration < 1 or self.done:
                del self.parent.Buff[self.name] 
                #it removes itseff from Actor.owned_buffs and Actor.Buff
            else:
                self.duration -= 1
                
    def __str__(self):
        tempstr = ''
        for k ,v in self.items():
            tempstr +=  '%s = %s ' % (k, v) 
        return tempstr

    
class AditionalDMG(Buff):
    def __init__(self, parent, target, damage):
        super(AditionalDMG, self).__init__(parent, target)
        self.name = 'AddDMG'
        self.type = 'Buff'
        self.name = 'More Damge'
        self.damage = damage
        #Poner un comando no inventado
        self.done = False
        self.duration = 2
    def on_damage(self):
        #you need to do this more simple and generic
        if not self.done:
            self.damage *= (100.0 / (100.0 + self.target.target['res']))
            self.target.trigger.on_damage(self.damage)
            self.target.target.trigger.on_damaged(self.damage)
            self.target.target.current_hp -= self.damage
            self.done = True
            
class ProtectBuff(Buff):
    def __init__(self,parent,target):
        Buff.__init__(self,parent,target)
        self.name = 'Protect'
        self.type = 'Buff'
        self.name = 'Iron Protection'
        self.done = False
        self.duration = 3
        
    def on_focus(self, command):
        if not self.done and command.is_physical() and self.parent.can_move():
            command.targets = [self.parent]
            self.done = True
            
    def on_combat_end(self):
        del self.parent.Buff[self.name] #it removes itseff from Actor.owned_buffs and Actor.Buff

#Execute if this file is run as a script and not imported as a module
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)
    
    buffs = BuffDict()
    d3 = StatsDict(str = 2,mag = 6, hp = 100)
    d4 = StatsDict(str = 2,mag = -12, hp = 100, res = 4)
    buffs['enhance'] = d3
    buffs['debuff'] = d4
    del buffs['debuff']
    logging.info(buffs.total_bonus)