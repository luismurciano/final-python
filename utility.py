#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
import logging
from error import InvalidSlotError
ALLOWED_STATS = ['hp','str','def','spd','evd','hit','mag','res']
class SummableDict(dict):
    """Dictionary summable and restrictive.
    Features:
    Only accepts keys if their value is in(allows) None=all
    It's summable returning a dict this same key and values added
    """
    def __init__(self, allow = None, dict = None, **kwargs):
        super(SummableDict, self).__init__(self)
        self.__allows__ = allow
        self.update(dict)
        self.update(kwargs)
    
    def __setitem__(self, key, item): 
        if self.__allows__ == None or key in self.__allows__:
            super(SummableDict, self).__setitem__(key, item)
        else:
            raise InvalidSlotError(key)
            
    def update(self, check=None, **kwargs):
        if check is not None :
            if self.__allows__ != None:
                for key in check.keys():
                    if not key in self.__allows__:
                        raise InvalidSlotError(key)
            
                super(SummableDict, self).update(check)
    
    def __add__(self, other):
        """Adds the values of a to b
        TODO: More efficient
        """
        temp = self.copy()
        if not issubclass(other.__class__, SummableDict):
            other = SummableDict(self.__allows__, other)
        for k, v in other.items():
            try:
                if type(temp[k]).__name__ == 'str' or type(v).__name__ == 'str':
                    listtemp = []
                    listtemp.append(temp[k])
                    listtemp.append(v)
                    temp[k] = listtemp
                    
                else:
                    try:
                        if type(temp[k]).__name__ == 'list' and type(v).__name__ == 'list':
                            
                            templist = temp[k] + v
                            temp[k] = templist
                        else:
                            temp[k] += v
                    except TypeError:
                        listtemp = []
                        listtemp.append(temp[k])
                        listtemp.append(v)
                        temp[k] = listtemp
                    
            except KeyError:
                temp[k] = v
                
            finally:    
                if temp[k] == 0 or temp[k] == []:
                    del temp[k]
        return temp
    
    def __sub__(self, other):
        """Adds the values of a to b
        If the total value is 0 removes that Key
        TODO: More efficient
        """
        temp = SummableDict(self.__allows__, self)
        if not issubclass(other.__class__, SummableDict):
            other = SummableDict(self.__allows__, other)
        for k, v in other.items():
            try:                
                templist = temp[k] - v
                temp[k] = templist
            except TypeError:
                if type(temp[k]).__name__ == 'list' and type(v).__name__ == 'list':
                    for item in v:
                        temp[k].remove(item)
                    
            except KeyError:
                temp[k] = -v
                
            finally:

                if temp[k] == 0 or temp[k] == []:
                    del temp[k]
        return temp

    def clear(self): 
        super(SummableDict, self).clear()          
    
    def copy(self):                             
        if self.__class__ is dict:          
            return dict(self)                       
        return SummableDict(allow = self.__allows__, dict = self)                 
    
    def keys(self): 
        return super(SummableDict, self).keys()     
    
    def items(self): 
        return super(SummableDict, self).items()  
    
    def values(self): 
        return super(SummableDict, self).values()
  
class StatsDict(SummableDict):
    def __init__(self, Dict = None, **kwargs):
        self.allows = ALLOWED_STATS
        super(StatsDict,self).__init__(self.allows, Dict, **kwargs)
    
    def __str__(self):
        tempstr = ''
        for k ,v in self.items():
            tempstr +=  '%s = %s ' % (k, v) 
        return tempstr

class StatsContainer(SummableDict):
    """This class contais an StatsDict and total bonus, you can add it
    to others StatsDict 
    """
    def __init__(self,allows = None, stats = StatsDict(), Dict = None, **kwargs):
        self.allows = allows
        super(StatsContainer,self).__init__(self.allows, Dict, **kwargs)
        self.total_bonus = stats
   
    def __setitem__(self, key, item):
        """TODO: More efficient
        """
        logging.debug('Accessing to %s from: %s'%( key, self.__class__))
        #If Element allready exists
        #First we must subs its value before adding a new one
        if self.has_key(key):
            if issubclass(self[key].__class__, StatsContainer):
                self.total_bonus -= self[key].total_bonus
            else:
                self.total_bonus -= self[key]
        
        super(StatsContainer, self).__setitem__(key, item)
        if issubclass(self[key].__class__, StatsContainer):
            self.total_bonus += self[key].total_bonus
        else:
            self.total_bonus += self[key]
    
    def __delitem__(self, key): 
        if issubclass(self[key].__class__, StatsContainer):
            self.total_bonus -= self[key].total_bonus
        else:
            self.total_bonus -= self[key]
        super(StatsContainer, self).__delitem__(key)

    def __add__(self, other):
        #TODO: Revisar ahora mismo devuelve un StatsDict no un StatsContainer
        return self.total_bonus + other.total_bonus
    def __sub__(self, other):
        return self.total_bonus - other.total_bonus

class ActorContainer(StatsContainer):
    pass
#Execute if this file is run as a script and not imported as a module
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)
    d1 = SummableDict(['hp','str','def','spd','evd','hit','mag','res'],str = 2)
    d2 = SummableDict(['hp','str','def','spd','evd','hit','mag','res'],{'str':3,'mag':5,'res':2})
    dsum = d1+d2
    print dsum
    
    d3 = StatsDict(str = 2,mag = 6)
    d4 = StatsDict({'str':-3,'mag':5,'res':2})
    dsum = d3+d4
    print dsum
    allowed = ['hp','str','def','spd','evd','hit','mag','res']
    
    d1 = SummableDict(allowed,str = [1,2,3],mag = 2)
    d2 = SummableDict(allowed,{'str':[5,6,7],'mag':5,'res':2})
    logging.info( d1 )
    logging.info( d2 )
    dsum = d1+d2
    logging.info('d1 + d2 = %s' % dsum )
    logging.info( d1 )
    logging.info( d2 )
    
    logging.info( 'dsum - d1 = %s' % (dsum - d1) )