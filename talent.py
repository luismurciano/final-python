#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
class Talent (object):
    """
    TODO: implement as a tree
    This class define the talents of every job in game.
    Talents adds Stats, Buffs, or Commands
    """
    def __init__(self):
        
        self.name = "None"
        self.descripton = "No Description"
        self.lvl = 0
        self.lvl_max = 1
        self.bonus = {}
    def level_up(self):
        """Adds a level to the talent"""
        if self.lvl < self.lvl_max:
            self.lvl += 1
            return True
        else:
            return False
    def add_bonus(self):
        """
        This function returns the stat,buff or command bonus including its lvl
        """
        return self.bonus
        
    def level_down(self):
        """Subs a level to the talent"""
        if self.lvl > 0:
            self.lvl -= 1
            return True
        else:
            return False
    
    def __str__(self):
        return "Talent %s level:%s" % (self.name, self.lvl)
        