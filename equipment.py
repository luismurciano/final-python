#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
from utility import StatsContainer
import logging

"""
TODO: Make custom exceptions for no slots
Revisar
"""
class Equipment(StatsContainer):
    
    def __init__(self, Dict = None, **kwargs):
        self.allows = ['hand1','head','hand2','armor','boots','ring']
        super(Equipment,self).__init__(self.allows, Dict, **kwargs)
        
        
#Execute if this file is run as a script and not imported as a module
if __name__ == "__main__":
    from utility import AdditiveDict
    logging.basicConfig(level=logging.DEBUG)
    d1 = Equipment()
    d1['hand2'] = dict(str = 3,res = 3)
    d1['head'] = dict(str = 3,res = 3,hp = 100)
    d1['head'] = dict(str = 3,res = 3,hp = 110)
    print d1.total_bonus
    
    del d1['head']
    print d1.total_bonus
    allowed = ['hp','str','def','spd','evd','hit','mag','res']
    
    d2 = AdditiveDict(allowed,str = 10,mag = 2)
    logging.info(d1-d2)