#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from random import randint
from actor import Actor

class Character(Actor):
    _LevelMax = 20      #this is the current level cap, I will adjust this with the number of content available
    #these mark the required amount of exp to level up
    #I calculated the curve myself in order to provide a fast, yet balanced
    #equation for leveling up.  There shouldn't be too much grind, but there
    #should be enough that you don't get bored by being over powered too easily
    _expCalc = staticmethod(lambda x: int(8.938*x**2.835))
    
    def __init__(self, name):
        
        super(Character, self).__init__(name)
        self.inventory = {}
        self.equipment = {}
        self.level = 1
        self.exp = 0
        self.points = 0
        self.talents = []
        
    def level_up(self):
        if self.exp >= Character._expCalc(self.level):
            self.exp = self.exp - Character._expCalc(self.level)
            self.level += 1
            self.points += 5
            return True
        return False