#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
"""
Commands are the core of the combat system.
All combat related functions are delegated to commands. This way we have a 
centralizated and easy to test combat module.
"""
import random
from math import *
from actor import Actor
from character import Character
import logging
from error import ActionStoppedError
PHYSICAL = 0
MAGICAL = 1
#command objects are specifically for the battle system
class Command(object):
    """Command used in battle, other commands must derive from this class 
    """
    def __init__(self, actor):
        """
        :param Actor actor: Actor who perform the command
        :param list targets: List of Actors who the command will apply
        :rtype: Command
        """
        self.name = 'Default'
        self.description = ''
        self.parent = actor        #actor who is performing the command
        self.targets = []          #Must be a List
        self.max_targets = None
        
        self.cost = 0
        self.cooldown = None
        self.last_time_used = None
        self.done = False
    
    def aviable(self):
        """Check if the parent meet the requierements before execute
            this function is very important cause is checked several times
            during de Command.execute(targets)            
        """
        has_ap = self.parent.ap <= self.cost
        is_incapatated = self.parent.debuff.has_key('incapacitated')
        return has_ap and not is_incapatated

    def execute(self, targets):
        """Command to preform
        """
        try:
            self.targets.extend(targets)
        except TypeError:
            logging.error('Target must be a list')

    def __clean(self):
        """Post execution effects such as remove buff and decrementing AP
        """
        self.targets = []
        self.parent.ap -= self.cost

    
    def __str__(self):
        """Representation example: Attack (4)

        """
        return "%s (%s)" % (self.name, self.cost)

class Attack(Command):
    """
    Basic Attack Command esta bastante mas sobrecargada de lo que pensaba
    TODO: Decide which stat is applied to the damage 
    """
    def __init__(self, actor):
        super(Attack, self).__init__(actor)
        self.max_targets = 1
        self.type = PHYSICAL
        self.target = self.targets[0]
        self.damage = None              #Used just to retrieve modifiers from
                                        #from triggers
        self.dmg_command = 0
        
        self.dmg_actor = 0
        if self.type == PHYSICAL:
            self.dmg_actor = self.parent.stats['atk']
        elif self.type == MAGICAL:
            self.dmg_actor = self.parent.stats['mag']

        self.dmg_factor = 1
          
    def __on_cast(self):
        """Calls any buff active when actor cast a command y
            when targets is being target of a casted command
        """
        logging.info('%s is using %s on %s' % (self.parent.name, self.name, self.target.name))
        self.parent.trigger.on_cast(self)
        self.target.trigger.on_focus(self)
            
    def __hits(self):
        """returns True if hit and False if not hit """
        if self.parent.stats['hit'] < random.randint(0, 99):
            logging.debug('%s hits %s' % (self.parent.name, self.target.name))
            self.parent.trigger.on_hit()
            self.target.trigger.on_hitted()
            return True
        
        else:
            logging.info('%s missed %s' % (self.parent.name, self.target.name))
            self.parent.trigger.on_miss()
            self.target.trigger.on_missed()
            return False

    def __tgt_dodges(self):
        """returns True if Actor dodges the incoming attack
        """
        if self.target.stats['dodge'] < random.randint(0, 99):
            self.parent.trigger.on_dodged()
            self.target.trigger.on_dodge()
            logging.info('%s dodges %s attack' % (self.target.name, 
                                                  self.parent.name))
            return True
        else:
            logging.info('%s\'s %s impacts on %s' % (self.parent.name,
                                                     self.name, 
                                                     self.target.name))
            return False  
        
    def __armor_mitigation(self):
        """
        Returns the factor of damage dealt once armor applied
        
        :Note: armor used to calculate is self.target.['def']
        :rtype: integer
        :returns: the factor of damage reduced by resistance
        """
        armor = self.target.stats['def']
        
        return (100.0 / (100.0 + armor))

    def __deal_damage(self):
        """
        Returns the amount of damage dealt by the player
        TODO: what if 0 dealt
        """
        self.damage = (self.dmg_command + self.dmg_actor) * self.dmg_factor
        #damage = int(self.parent.stats['str'] * self.__armor_mitigation())
        if self.damage > 0:
            self.damage *= self.__armor_mitigation()
            #TODO: Change to be generic _mitigation()
            self.parent.trigger.on_damage(self.damage)
            self.target.trigger.on_damaged(self.damage)
        elif self.damage < 0:
            self.parent.trigger.on_heal(self.damage)
            self.target.trigger.on_healed(self.damage)
            
        self.target.current_hp -= self.damage    #THIS IN FACT IS THE RESULT
        
        logging.debug('%s deal %i to %s' % (self.parent.name, 
                                            abs(self.damage), 
                                            self.target.name))
        return 
    
    def execute(self, targets):
        self.__execute(targets)
        
    def __execute(self,targets):
        """This function should be reused for every physical attack.
            Inherit classes must not overwrite this.
        """
        super(Attack, self).execute(targets)
        if self.max_targets != len(targets):
            logging.error('%s is attacking too many targets' % self.parent.name)
            self.target = self.targets[0]
            
        if not self.aviable():   
            logging.info('%s can\'t perform the action')
            return False
        
        try:            
            """NOTE:the code raises an ActionStoppedError at anytime a command is interrupted
            by other command
            """    
        
            self.__on_cast()
        
            if self.__hits():
                
                if not self.__tgt_dodges():
                    self.__deal_damage()
                else:
                    pass
                
        except ActionStoppedError, e:
            logging.info('Command: %s interrumped by %s' % (self.name, e.value))
        
        finally:
            self.__clean()
     
    def __clean(self):    
        super(Attack, self).__clean()
        self.target = None
        
    def __str__(self):
        str = self.name
        logging.debug(self.name)

class Cast(Command):
    def __init__(self, actor, skill):
        self.parent = actor
        self.skill = skill
        self.cost = skill.cost

    def execute(self):
        self.parent.damage = (self.parent.mag + self.skill.damage) - (self.parent.target.res * 1.368295)


#difference between UseItem and Cast is UseItem's damage is constant
class UseItem(Cast):
    def __init__(self, actor, item):
        self.parent = actor
        self.item = item

    def execute(self):
        self.parent.damage = self.skill.function.damage

    #removes item instead of ap
    def reset(self):
        self.parent.family.inventory.removeItem(self.item, 1)

#when a character defends they gain the normal amount of ap per turn (20%)
#but their def is multiplied by 250%
class Defend(Command):
    def __init__(self, actor):
        super(Defend, self).__init__(actor)

    def execute(self):
        self.parent.defn *= 2.5

    def reset(self):
        self.parent.defn /= 2.5

    def __str__(self):
        return "Defend (Def * 2.5)"

#when an actor boosts instead of defends,
#their def is halved but they get full ap the next turn
class Boost(Command):
    def __init__(self, actor):
        super(Boost, self).__init__(actor)

    def execute(self):
        self.parent.defn *= .5

    def reset(self):
        self.parent.ap = self.parent.maxap
        self.parent.defn *= 2

    def __str__(self):
        return "Boost (Full ap)"

#Guns and Bows have 4 different firing modes
#Single is default for all
#Guns can have either Double shot or Burst+Auto firing modes
#Bows can also have Double
#since this is heavily dependent on equipment,
#  only characters should use this command
class Shoot(Command):
    def __init__(self, actor, style = 0):
        super(Shoot, self).__init__(actor)

        self.animation = self.parent.equipment[self.parent.hand].attackAnimation

        self.style = style    #4 different styles of attack
                    #  0 - single
                    #  1 - double, 50% accuracy but 200% stronger
                    #  2 - burst, 75% accuracy, 3 hits
                    #  3 - auto, 50% accuracy, 5 hits

        #because guns have a lot of recoil and bows you have to pull back hard on to fire
        # on average they will use more energy to fight with than close range weapons
        #additionally the different types of firing take more energy
        if self.style == 1:
            self.cost = 50
        elif self.style == 2:
            self.cost = 55
        elif self.style == 3:
            self.cost = 65
        else:
            self.cost = 40

    #get the amount of damage dealt per hit
    def getDamage(self):
        factor = (self.parent.proficiency*2)
        if self.style == 1 or self.style == 3:
            hit = factor - self.parent.target.evd >= self.parent.target.evd*3
        elif self.style == 2:
            hit = factor - self.parent.target.evd >= self.parent.target.evd*2
        else:
            hit = factor - self.parent.target.evd >= self.parent.target.evd*1.5

        if hit:
            return self.parent.str - (self.parent.target.defn * 1.368295)
        else:
            return 0

    def execute(self):

        damage = 0
        #double shot
        if self.style == 1:
            damage += 2*self.getDamage()
        #burst
        elif self.style == 2:
            for i in range(3):
                damage += self.getDamage()
        #auto
        elif self.style == 3:
            for i in range(5):
                damage += self.getDamage()
        #single
        else:
            damage += self.getDamage()
        damage = max(0, int(damage))

        self.parent.damage = damage

        self.animation = self.parent.equipment[self.parent.hand].attackAnimation

        self.animation.currentFrame = 0
        self.animation.setParent(self.parent.target.getSprite())
        if isinstance(self.parent.target, Character):
            self.animation.flip = -1
        else:
            self.animation.flip = 1

    def __str__(self):
        if self.style == 1:
            name = "Double"
        elif self.style == 2:
            name = "Burst"
        elif self.style == 3:
            name = "Auto"
        else:
            name = "Single"
        return "%s ap: %i" % (name, self.cost)

class Flee(Command):
    def __init__(self, actor, party, formation):
        super(Flee, self).__init__(actor)
        self.success = False

        self.party = party
        self.formation = formation
        self.getDifficulty = party.deficulty
        self.cost = 50    #takes 50 ap investment to flee

    #chance of fleeing should be determined by comparing party's stat's to the formations
    #it should be harder to flee when the party is almost dead and easier to flee when the
    #enemies formation is almost vanquished
    def execute(self):
        self.success = (random.randint(self.party.avgSpd, 100) / self.getDifficulty(self.formation, self.party.getAlive())) > 50

    #costs all ap if not successful
    def reset(self):
        if not self.success:
            self.parent.ap = 0

    def __str__(self):
        return "Flee"
# Execute if this file is run as a script and not imported as a module

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)