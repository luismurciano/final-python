#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from utility import StatsDict, StatsContainer
from buff import BuffDict
import logging
import random

class Actor(StatsContainer):
    """This class stores actors information from db during the battle """

    def __init__(self, stats , commands = []):
        super(Actor,self).__init__(allows = None,stats = stats, Dict = stats)
        self.name = 'NoName'
        self.commands = commands
        
        #Its a dict because you cant have 2 identical buffs
        self.buffs = {}
        self.owned_buffs = {}
        """
        Base stats
        hp    hit points
        str   Strength
        def   Defense
        spd   speed
        evd   evasion
        hit   Hit
        mag   magic strength 
        res   magic defense (resistance)
        """
        
        #used in battle
        self.max_ap = 10
        self.ap = 0
        self.current_hp = self.total_bonus['hp']
        self.incap = False      #actors become incapacitated when their hp reaches 0
        
        self.command = None
        self.active = False

        #are they casting a spell or technique, if they are, which one
        self.cast = False
        self.spell = None
        
        #the enemy or ally being targeted
        self.targets = None
        self.damage = 0
        self.position = (0,0)       #position where the sprite is set
    
    ############## BEGIN OF TRIGGERS ############     
    def on_cast(self, *args ,**kwords):
        try:
            triggers = self['on_cast']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_cast trigger')
        
    def on_focus(self, *args ,**kwords):
        try:
            triggers = self['on_focus']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_focus trigger')

    def on_damage(self, *args ,**kwords):
        try:
            triggers = self['on_damage']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_damage trigger')

    def on_damaged(self, *args ,**kwords):
        try:
            triggers = self['on_damaged']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_damaged trigger')

    def on_heal(self, *args ,**kwords):
        try:
            triggers = self['on_heal']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_heal trigger')

    def on_healed(self, *args ,**kwords):
        try:
            triggers = self['on_healed']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_healed trigger')

    def on_dodge(self, *args ,**kwords):
        try:
            triggers = self['on_dodge']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_dodge trigger')

    def on_dodged(self, *args ,**kwords):
        try:
            triggers = self['on_dodged']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_dodged trigger')

    def on_miss(self, *args ,**kwords):
        try:
            triggers = self['on_miss']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_miss trigger')

    def on_missed(self, *args ,**kwords):
        try:
            triggers = self['on_missed']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_missed trigger')

    def on_hit(self, *args ,**kwords):
        try:
            triggers = self['on_hit']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_hit trigger')

    def on_hitted(self, *args ,**kwords):
        try:
            triggers = self['on_hitted']
            if type(triggers).__name__ == 'list':
                for item in triggers:
                    item(*args ,**kwords)
            else:
                triggers(*args ,**kwords)
        except KeyError:
            logging.info('%s don\'t have on_hitted trigger')

                   
    def add_command(self,command):
        cmd = command(self)
        self.commands[ cmd['name'] ] = cmd
        return True
    
    ############## END OF TRIGGERS ############   
     
    def use_command(self,command_name,targets):
        """
        Calls a command using an Array of Targets
        """
        effect = None
        try:
            command = self.commands[command_name]
        except KeyError:
            logging.error( "The command %s doesn`t exist" % command_name )
        else:   
            if command.aviable():
                logging.info("Using %s:" % command_name)
                effect = command.execute(targets)
            else:
                logging.info( "You can't use %s " % command_name )
                
        return effect
    
    def init_battle(self):
        """Called at battle's begin
        Sets HP, AP and init other atributes
        """
        self.current_hp = self.total_bonus['hp']
        self.ap = min(self.max_ap/3 + random.randint(0, self.max_ap), self.max_ap)
        self.active = False

    def turn_start(self):
        self.use_command(self.command.name, self.targets)

    #ends the enemy's turn
    def turn_end(self):
        """Ends Trun and checks the buffs
        """
        self.ap += self.max_ap / 5
        self.ap = min(self.ap, self.max_ap)
        
        #resets defense and other command with modifiers
        self.command.reset()
        
        #updates the buff created by the Actor
        for buff in self.owned_buffs.values():
            buff.update_buff()

        self.targets = None
        self.command = None
#Execute if this file is run as a script and not imported as a module
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Starting % s' % __file__)
    d1 = StatsDict(hp = 1)
    d3 = StatsDict(str = 2,mag = 6, hp = 100)
    d4 = StatsDict(str = 2,mag = -12, hp = 100)
    actor1 = Actor(d1)
    actor1['items'] = d3
    actor1['items'] = d4
    del actor1['items']
    buffs = BuffDict()
    d3 = StatsDict(str = 1000,mag = 1000, hp = 1000)
    buffs['enhance'] = d3
    
    actor1['buffs'] = buffs
    actor1['buffs']['yeaaaah'] = StatsDict(res = 100,mag = 100, hp = 100)
    actor1['buffs']['yeaaaah']['res'] = 1
    logging.info(actor1.total_bonus)